import React from 'react';
import Dialog, {   
    DialogContent,
    DialogContentText,
    DialogTitle,
} from '@material-ui/core/Dialog';

const TopicDialog = ({ topic, execute }) => {
    return (
        <Dialog
            fullScreen={false}
            open={topic != null}
            onClose={() => { execute({ type: 'MODAL_TOPIC', payload: null }) }}
            aria-labelledby="responsive-dialog-title">
            <DialogTitle id="responsive-dialog-title"> {topic || ''}</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Let Google help apps determine location. This means sending anonymous location data to
                    Google, even when no apps are running.
                </DialogContentText>
            </DialogContent>
        </Dialog>
    );
};

export default TopicDialog;