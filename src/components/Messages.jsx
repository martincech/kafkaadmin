import React from 'react';
import Table from '@material-ui/core/Table';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';

const Messages = props => {
    var messages = props.messages.map(item => {
        return (
            <TableRow key={item.topic + '_' + item.offset}>
                {
                    props.show ? <TableCell>{item.topic}</TableCell> : null
                }
                {/* <TableCell numeric>{item.partition}</TableCell> */}
                <TableCell>{item.offset}</TableCell>
                <TableCell>{item.value}</TableCell>
            </TableRow>
        )
    });

    return (
        <Table>
            <TableHead>
                <TableRow>
                    {
                        props.show ? <TableCell>Topic</TableCell> : null
                    }
                    {/* <TableCell numeric>Partition</TableCell> */}
                    <TableCell>Offset</TableCell>
                    <TableCell>Value</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {messages}
            </TableBody>
        </Table>
    );
}

export default Messages;