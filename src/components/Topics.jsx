import React from 'react';
import Button from '@material-ui/core/Button';
import Input from '@material-ui/core/Input';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import StopIcon from '@material-ui/icons/Stop';
import AddIcon from '@material-ui/icons/Add';


const Topics = props => {
    var topics = props.topics.map(t => {
        return (
            <div key={t} style={{ display: 'flex' }}>
                <Button
                    variant="contained"
                    onClick={() => { props.execute({ type: 'MODAL_TOPIC', payload: t }); }}
                    style={{ flex: 1, textTransform: 'initial' }}
                    color={props.topic === t ? "primary" : "default"}
                >
                    {t}
                </Button>
                <Button
                    style={{ minWidth: 32, width: 32 }}
                    variant="contained"
                    color="inherit"
                    aria-label="Settings"
                    onClick={() => { props.clickHandler(t) }}
                    color={props.selected.indexOf(t) == -1 ? "default" : "primary"}
                >
                    {
                        props.selected.indexOf(t) == -1 ? <PlayArrowIcon /> : <StopIcon />
                    }

                </Button>
            </div>
        );
    });

    return (
        <div>
            <div style={{ display: 'flex' }}>
                <Input
                    placeholder="Create topic"
                    style={{ flex: 1, textTransform: 'initial' }}
                />
                <Button
                    style={{ minWidth: 32, width: 32 }}
                    variant="contained"
                    color="inherit"
                    aria-label="Add"
                    color="primary"
                >
                    <AddIcon />
                </Button>
            </div>
            {topics}
        </div>
    );
}

export default Topics;