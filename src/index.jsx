import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import CloseIcon from '@material-ui/icons/Close';
import DeleteIcon from '@material-ui/icons/Delete';
import CachedIcon from '@material-ui/icons/Cached';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Topics from './components/Topics'
import Messages from './components/Messages'
import ActionReducer from './reducer/action-reducer'


import { ADD_MESSAGES, ADD_TOPICS, DELETE_TOPICS, CONSUME_TOPICS, GET_TOPICS, SET_CONFIG, MODAL_TOPIC, GET_HISTORY } from './actions/action-types';

class App extends Component {
    constructor() {
        super();
        this.state = {
            config: {
                zookeeper: ''
            },
            modalTopic: null,
            showMenu: true,
            topics: [],
            topic: null,
            consume: [],
            webSocket: null,
            messages: [],
            historyCount: 10,
            history: {}
        };
    }

    componentDidMount() {
        this.connect();
    };

    executeAction = (action) => {
        var newState = ActionReducer.Reduce(action, this.state);
        if (newState == null) return;
        if (action.type === MODAL_TOPIC && action.payload != this.state.topic)
            this.wsSendMessage(GET_HISTORY, { topic: action.payload, count: this.state.historyCount });
        this.setState(newState);
    }

    wsOnMessage = (wsMessage) => {
        var action = JSON.parse(wsMessage.data);
        if (action == null || action.type == null || action.payload == null) {
            console.error("Invalid action : ", action);
            return;
        }
        this.executeAction(action)
    }

    wsSendMessage = (type, payload) => {
        if (this.state.webSocket == null) return;
        this.state.webSocket.send(JSON.stringify({ type: type, payload: payload }));
    }

    getAllTopics = () => {
        this.wsSendMessage(GET_TOPICS, {});
    }

    getWSAddress = () => {
        var loc = window.location, new_uri;
        if (loc.protocol === "https:") {
            new_uri = "wss:";
        } else {
            new_uri = "ws:";
        }
        new_uri += "//" + loc.host;
        new_uri += loc.pathname + "/";
        return new_uri;
    }

    clearMessages = () => {
        this.setState({ messages: [] });
    }

    connect = () => {
        let webSocket = new WebSocket(this.getWSAddress());
        webSocket.onopen = () => {
            console.log("WebSocket OPENED.");
            this.getAllTopics();
        };
        webSocket.onclose = () => console.log("WebSocket CLOSED.");
        webSocket.onmessage = this.wsOnMessage;
        webSocket.onerror = this.wsOnError;
        this.setState({ webSocket: webSocket })
    }

    wsOnError = (err) => {
        console.error(err);
    }

    disconnect = () => {
        if (this.state.webSocket == null) return;
        this.state.webSocket.close();
        this.setState({ webSocket: null });
    }

    showMenu = () => {
        this.setState({ showMenu: !this.state.showMenu })
    }

    toggleTopic = (topic) => {
        var index = this.state.consume.indexOf(topic);
        var consume = index == -1 ? [...this.state.consume, topic] : this.state.consume.filter(f => f != topic);
        this.wsSendMessage(CONSUME_TOPICS, [...consume]);
        this.setState({ consume: consume });
    }

    changeCount = (ev) => {
        this.setState({ historyCount: ev.target.value });
        this.wsSendMessage(GET_HISTORY, { topic: this.state.topic, count: ev.target.value });
    }

    refresh = () => {
        this.wsSendMessage(GET_HISTORY, { topic: this.state.topic, count: this.state.historyCount });
    }

    render() {
        return (
            <div>
                <AppBar position="sticky">
                    <Toolbar>
                        <IconButton color="inherit" aria-label="Menu" onClick={this.showMenu}>
                            {
                                this.state.showMenu ? <CloseIcon /> : <MenuIcon />
                            }
                        </IconButton>
                        <Typography variant="title" color="inherit" style={{ flex: 1 }}>
                            brokers: {this.state.config.kafkaHost}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div style={{ display: 'flex' }}>
                    <div style={{ width: 220, display: this.state.showMenu ? '' : 'none' }}>
                        <Topics topics={this.state.topics} topic={this.state.topic} selected={this.state.consume} clickHandler={this.toggleTopic} execute={this.executeAction} />
                    </div>
                    <div style={{ flex: 1, display: this.state.topic == null ? 'none' : '', borderLeft: '3px solid black' }}>
                        <Toolbar style={{ backgroundColor: 'gray' }}>
                            <Typography variant="title" color="inherit" style={{ flex: 1 }}>
                                Browse {this.state.topic}
                            </Typography>

                            <Select value={this.state.historyCount} onChange={this.changeCount}>
                                <MenuItem value={10}>Ten</MenuItem>
                                <MenuItem value={50}>Fifty</MenuItem>
                                <MenuItem value={100}>Hundred</MenuItem>
                                <MenuItem value={1000}>Thousand</MenuItem>
                            </Select>
                            <IconButton color="inherit" aria-label="Menu" onClick={this.refresh}>
                                <CachedIcon />
                            </IconButton>
                        </Toolbar>
                        <Messages messages={this.state.history[this.state.topic] || []} />
                    </div>
                    <div style={{ flex: 1, borderLeft: '3px solid black' }}>
                        <Toolbar style={{ backgroundColor: 'gray' }}>
                            <Typography variant="title" color="inherit" style={{ flex: 1 }}>
                                Data stream
                            </Typography>
                            <IconButton color="inherit" aria-label="Menu" onClick={this.clearMessages}>
                                <DeleteIcon />
                            </IconButton>
                        </Toolbar>
                        <Messages messages={this.state.messages} show={true} />
                    </div>
                </div>
                {/* <TopicDialog topic={this.state.modalTopic} execute={this.executeAction} /> */}
            </div>
        );
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('container')
);
