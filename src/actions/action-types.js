export const ADD_MESSAGES = "ADD_MESSAGES";

export const ADD_TOPICS = "ADD_TOPICS";
export const DELETE_TOPICS = "DELETE_TOPICS";

export const CONSUME_TOPICS = "CONSUME_TOPICS";
export const GET_TOPICS = "GET_TOPICS";

export const SET_CONFIG = "SET_CONFIG";

export const MODAL_TOPIC = "MODAL_TOPIC";

export const GET_HISTORY = "GET_HISTORY";
