import { ADD_MESSAGES, ADD_TOPICS, DELETE_TOPICS, CONSUME_TOPICS, GET_TOPICS, SET_CONFIG, MODAL_TOPIC, GET_HISTORY } from '../actions/action-types';

class ActionReducer {

    static sort = (a, b) => {
        var nameA = a.toUpperCase();
        var nameB = b.toUpperCase();
        if (nameA < nameB) {
            return -1;
        }
        if (nameA > nameB) {
            return 1;
        }
        return 0;
    }

    static Reduce(action, state) {

        var newState = {};
        switch (action.type) {
            case ADD_MESSAGES:
                newState = { messages: [...action.payload, ...state.messages] };
                break;
            case GET_HISTORY:
                newState = { history: Object.assign({}, state.history, action.payload) };
                break;
            case GET_TOPICS:
                newState = { topics: [...Object.keys(action.payload)].sort(this.sort) };
                break;
            case ADD_TOPICS:
                newState = { topics: [...action.payload, ...state.messages] };
                break;
            case DELETE_TOPICS:
                newState = { messages: state.topics.filter((t) => action.payload.indexOf(t) == -1) };
                break;
            case SET_CONFIG:
                newState = { config: Object.assign({}, action.payload) };
                break;
            case MODAL_TOPIC:
                newState = {
                    modalTopic: action.payload,
                    topic: action.payload == state.topic ? null : action.payload
                };
                break;
            default:
                console.warn("Uknown action : ", action);
                break;
        }
        return newState;
    }
}

export default ActionReducer;