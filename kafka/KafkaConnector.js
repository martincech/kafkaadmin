var kafka = require('kafka-node');
var _ = require('lodash');
const uuidv4 = require('uuid/v4');

class KafkaConnector {
    constructor(kafkaHost, consumerCallBack, historyCallback) {

        this.settings = {
            groupId: uuidv4(),//consumer group id, default `kafka-node-group`
            // Auto commit config
            autoCommit: true,
            autoCommitIntervalMs: 5000,
            // The max wait time is the maximum amount of time in milliseconds to block waiting if insufficient data is available at the time the request is issued, default 100ms
            fetchMaxWaitMs: 100,
            // This is the minimum number of bytes of messages that must be available to give a response, default 1 byte
            fetchMinBytes: 1,
            // The maximum bytes to include in the message set for this partition. This helps bound the size of the response.
            fetchMaxBytes: 1024 * 1024,
            // If set true, consumer will fetch message from the given offset in the payloads
            fromOffset: false,
            // If set to 'buffer', values will be returned as raw buffer objects.
            encoding: 'utf8',
            keyEncoding: 'utf8'
        };


        this.topics = [];
        this.kafkaHost = kafkaHost;
        this.consumerCallBack = consumerCallBack;
        this.client = new kafka.KafkaClient({ kafkaHost: kafkaHost });
        this.offset = new kafka.Offset(this.client);
        this.consumer = new kafka.Consumer(this.client, [], this.settings);
        this.consumer.on('message', (m) => {
            this.consumerCallBack(m);
        });
        this.consumer.on('error', (err) => { console.warn(err); })

        this.highWaterOffset = -1;
        this.cache = [];
        this.htopic = null;
        this.historyCallback = historyCallback;
        this.hconsumer = new kafka.Consumer(this.client, [], Object.assign({}, this.settings, { groupId: uuidv4() }));
        this.hconsumer.on('message', (m) => {
            this.cache.push(m);
            if (this.highWaterOffset === -1) this.highWaterOffset = m.highWaterOffset;
            if (m.offset < this.highWaterOffset - 1) return;
            var result = {};
            result[this.topic] = [...this.cache].sort((a, b) => { return b.offset - a.offset });
            this.historyCallback(result);
            this.removeTopic();
        });
        this.hconsumer.on('error', (err) => { console.warn(err); })
    }

    loadMetadataForTopics(cb) {
        this.client.loadMetadataForTopics([], (error, results) => {
            if (error) {
                console.error(error);
                cb(null);
                return;
            }
            if (cb != null) cb(_.get(results, '1.metadata'));
        });
    }

    consumeTopics(t) {
        const toRemove = this.topics.filter(f => t.indexOf(f) === -1);
        const toAdd = t.filter(f => this.topics.indexOf(f) === -1);
        this.topics = [...t];

        if (toRemove.length > 0) {
            this.consumer.removeTopics(toRemove, (err, added) => {
                console.log(err, added);
            });
        }
        if (toAdd.length > 0) {
            this.offset.fetch(
                //  [{ topic: 't', partition: 0, time: Date.now(), maxNum: 1 }]
                toAdd.map(m => ({ topic: m, partition: 0, time: -1, maxNum: 1 }))
                , (err, data) => {
                    console.log(err, data);
                    this.consumer.addTopics(toAdd.map(m => ({ topic: m, offset: data[m][0] })), (err, added) => {
                        console.log(err, added);
                    }, true);
                });
        }
    }

    close() {
        this.client.close(() => { console.log("Kafka Client closed!") });
    }


    removeTopic() {
        if (this.topic == null) return;
        this.hconsumer.removeTopics([this.topic], (err, added) => {
            console.log("removeTopic ", err, added);
            this.topic = null;
            this.cache = [];
        });
    }

    addTopic(topic, offset) {
        if (topic == null) return;
        this.highWaterOffset = -1;
        this.hconsumer.addTopics([{ topic: topic, offset: offset }], (err, added) => {
            console.log("addTopic ", err, added);
            this.topic = topic;
            this.cache = [];
        }, true);
    }

    loadHistory(topic, count) {
        this.removeTopic();
        this.offset.fetch(
            [{ topic: topic, partition: 0, time: -1, maxNum: 1 }]
            , (err, data) => {
                if (err != null) {
                    console.warn(err);
                    return;
                }
                console.log(data);
                var offset = data[topic][0][0];
                if (offset === 0) {
                    console.log("No data in topic : " + topic);
                    return;
                }
                offset = offset < count ? 0 : offset - count;
                this.addTopic(topic, offset);
            });
    }
}

module.exports = KafkaConnector;