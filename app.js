const SocketServer = require('ws').Server;
const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const index = require('./routes/index');
const KafkaConnector = require('./kafka/KafkaConnector');

const config = require('./config');

const app = express();
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

/**
 * uncomment after placing your favicon in /public
 * app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
 */
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'dist')));

app.use('/', index);
// app.use('/kafka', kafka);

const server = app.listen(config.port, () => {
  console.log(`node.js static, REST server and websockets listening on port: ${config.port}`);
});

const wss = new SocketServer({ server });

const invalidMessage = (message) => {
  console.error("Invalid message : ", message);
};

// init Websocket ws and handle incoming connect requests
wss.on('connection', (ws) => {
  console.log('WS Client connected.');

  send = (type, data) => {
    ws.send(JSON.stringify({ type: type, payload: data }));
  };
  send('SET_CONFIG', { kafkaHost: config.kafkaHost });
  var kc = new KafkaConnector(config.kafkaHost,
    (m) => send("ADD_MESSAGES", [m]),
    (data) => {
      ws.send(JSON.stringify({ type: "GET_HISTORY", payload: data }));
    });
  ws.on('error', (e) => {
    console.error('WS error : ', e);
    kc.close();
  });

  ws.on('message', (message) => {
    var message = JSON.parse(message);
    if (message == null) return console.error('WS error : ', message);
    if (message.type)
      switch (message.type) {
        case 'GET_TOPICS':
          kc.loadMetadataForTopics((data) => {
            ws.send(JSON.stringify({ type: "GET_TOPICS", payload: data }));
          });
          break;
        case 'CONSUME_TOPICS':
          kc.consumeTopics(message.payload);
          break;
        case 'GET_HISTORY':
          kc.loadHistory(message.payload.topic, message.payload.count);
          break;
        default:
          console.error('WS error : ', message);
          break;
      }
  });
});
module.exports = app;
