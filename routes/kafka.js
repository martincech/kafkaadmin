var express = require('express');
var router = express.Router();
var _ = require('lodash');
var kafka = require('kafka-node');

const client = new kafka.KafkaClient({
  kafkaHost: 'localhost:9092'
});
client.once('connect', function () {
  console.log("connected")
});

router.get('/', (req, res) => {
  client.loadMetadataForTopics([], function (error, results) {
    if (error) {
      return console.error(error);
    }
    return res.json(_.get(results, '1.metadata'))
  });
})

router.post('/create', (req, res) => {
  var kafka = require('kafka-node'),
    Producer = kafka.Producer,
    client = new kafka.Client("localhost:2181"),
    producer = new Producer(client);
  producer.on('ready', function () {
    producer.createTopics([req.body.topic], false, function (err, data) {
      console.log(err, data);
      res.json({
        topic: req.body.topic
      });
    });
    producer.on('error', function (err) {})
  });
})

module.exports = router;