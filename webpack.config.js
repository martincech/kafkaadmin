const path = require('path');
// const webpack = require('webpack');

module.exports = {
  entry: './src/index.jsx',
  output: { path: __dirname + '/dist', filename: 'script.min.js' },
  resolve: { extensions: ['.js', '.jsx'] }, 
  module: {
    loaders: [
      {
        test: /.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['es2015', 'react'],
          plugins: ['transform-class-properties', 'react-html-attrs'],
        },
      },
    ],
  },
};
